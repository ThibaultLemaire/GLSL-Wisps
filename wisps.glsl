// Inspired by http://glslsandbox.com/e#45503.0

#define PI 3.14159265359

precision highp float;

uniform float u_time;
uniform vec2 u_resolution;

struct Polar {
	float angle;
	float radius;
};

Polar polar(vec2 pos) {
	return Polar(
		atan(pos.x, pos.y),
		length(pos)
	);
}

Polar rotate(float amount, Polar p) {
	p.angle = mod(p.angle + amount, 2.0 * PI) - PI;
	return p;
}

float rand(float x) {
    return(fract(sin(x)*1e4));
}

float noise(float x) {
    float i = floor(x);
    float f = fract(x);
    return mix(rand(i), rand(i + 1.0), smoothstep(0.0, 1.0, f));
}

float wispShape(float bump, Polar p) {
	return (1.0 + noise(p.angle * 1.186) * bump) / p.radius;
}

vec3 orbit(float time, vec2 amplitude) {
    return vec3(
		vec2(sin(time), cos(time)) * amplitude,
		-cos(time) + 1.2);
}

vec3 wisp(vec2 st,
		  vec2 coord,
		  vec3 color,
		  vec3 orbit,
		  float size,
		  float rotation,
		  float bump) {
	return normalize(color)
		* wispShape(bump, rotate(rotation, polar(coord-st+orbit.xy)))
		* size
		* orbit.z;
}

void main(void) {
	vec2 st = gl_FragCoord.xy / u_resolution.y;
	st = st * 2.0 - 1.0;

	float r_time = u_time * 0.771;

	vec3 color = vec3(0.0);
	color += wisp(st,
                  vec2(0.040,0.000),
                  vec3(0.985,0.695,0.305),
                  orbit(r_time, vec2(0.840,0.180)),
                  0.044,
				  r_time * 5.0,
				  0.1);

	gl_FragColor = vec4(color, 1.0);
}
